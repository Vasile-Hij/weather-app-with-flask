from flask_login import UserMixin
from . import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String, unique=True)
    password = db.Column(db.String(100))
    city = db.Column(db.String(50), nullable=False)
    authenticated = db.Column(db.Boolean, default=False)

    words = db.Column(db.String)
    number_of_words = 0

    def __init__(self, email, name, password, city):
        self.email = email
        self.password = password
        self.name = name
        self.city = city

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False
