#!/bin/usr/env python3
from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required
from .__init__ import db
from .user import User

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET'])
def login():
    return render_template('login.html')


@auth.route('/signup')
def signup():
    return render_template('signup.html')


@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))

    login_user(user, remember=remember)
    return redirect(url_for('main.profile'))


@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')
    city = request.form.get('city')

    user = User.query.filter_by(email=email).first()

    if user:
        flash('Email address already exists!')
        return redirect(url_for('auth.signup'))

    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'),
                    city=city)
    try:
        db.session.add(new_user)
        db.session.commit()
    except:
        return 'Failed to add the { new_user } on database'
    return redirect(url_for('auth.login'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


def get_weather_data(city):
    url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&units=metric&appid=b3ae063d12815d4e1ca9ea9b3385b544'
    data = request.get(url=url).json()
    return data


@auth.route('/profile')
def get_city():
    cities = User.query.all()
    weather_data = []
    for city in cities:
        data = get_weather_data(city.name)
        print(data)
        weather = {
            'city': city.name,
            'temperature': data['main']['temp'] if 'main' and 'temp' in weather_data else None,
            'description': data['weather'][0]['description'] if 'weather' and 'description' in weather_data else None,
            'icon': data['weather'][0]['icon'] if 'weather' and 'icon' in weather_data else None
        }
        weather_data.append(weather)
    return render_template('main.profile') # pentru a rula  pagina `weather_data=weather_data` a fost scoasa


@auth.route('/profile', methods=['POST'])
def post_city():
    error_message = ''
    new_city = request.args.get('city') #form inlocuit cu args, insa nu afiseaza cautarea, doar mesajul
    if new_city:
        existing_city = User.query.filter_by(name=new_city).first()
        if not existing_city:
            new_city_data = get_weather_data(new_city)
            if new_city_data['cod'] == 200:
                new_city_obj = User(name=new_city)
                db.session.add(new_city_obj)
                db.session.commit()
            else:
                error_message = 'City does not exist in the world!'
        else:
            error_message = 'This city was checked. See below!'
    if error_message:
        flash(error_message, 'error')
    else:
        flash('City added successfully!')
    return redirect(url_for('main.profile'))


@auth.route('/delete/<name>')
def delete_city(name):
    city = User.query.filter_by(name=name).first()
    try:
        db.session.delete(city)
        db.session.commit()
    except:
        return 'Failed to delete city. Please try again!'
    flash(f'Successfully deleted {city.name}', 'success')
    return redirect(url_for('main.profile'))
