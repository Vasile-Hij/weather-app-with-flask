Weather application with user authentication in Flask
--
**Quick description:**
 
This application will display a web page with user authentication, then check the weather for your desired city.

**Tools**


* OpenWeatherMap API make the data collection
* SQLAlchemy stores the data for API and users registration
* Flask it is used for web 
* HTML/CSS/Jinja2 


**Functionality**

You need to register or log int to website, then you 
can check the weather by adding/deleting cities.